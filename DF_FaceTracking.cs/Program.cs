﻿/*******************************************************************************

INTEL CORPORATION PROPRIETARY INFORMATION
This software is supplied under the terms of a license agreement or nondisclosure
agreement with Intel Corporation and may not be copied or disclosed except in
accordance with the terms of that agreement
Copyright(c) 2013 Intel Corporation. All Rights Reserved.

*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace DF_FaceTracking.cs
{

     class DistractionDetection
    {
        public List<bool> frames = new List<bool>();
        public bool eyes_sleeping = false;
        public bool pose_sleeping = false;
        public bool phone_usage = false;
        double threshold = 0.65;

        //Print data and send and process it
        public void processData(PXCMFaceData.PoseEulerAngles angles, PXCMFaceData.ExpressionsData.FaceExpressionResult leftEye, PXCMFaceData.ExpressionsData.FaceExpressionResult rightEye)
        {
            Console.WriteLine("eyes_sleeping: " + eyes_sleeping);
            Console.WriteLine("pose_sleeping: " + pose_sleeping);
            Console.WriteLine("phone_usage: " + phone_usage);

            /*
            Console.WriteLine("PITCH: " + angles.pitch);
            Console.WriteLine("ROLL: " + angles.roll);
            Console.WriteLine("YAW: " + angles.yaw);
            Console.WriteLine("Right Eye closed: " + (rightEye.intensity>35));
            Console.WriteLine("Left Eye closed: " + (leftEye.intensity > 35));
            Console.WriteLine("Both Eyes closed: " + (rightEye.intensity > 35 && leftEye.intensity > 35));
            */

            pose_Sleeping(angles);
            eyes_Sleeping(leftEye, rightEye);
        }

        //Return int for according level of distraction
        public int isDistracted()
        {
            //Sleeping
            if (eyes_sleeping && pose_sleeping)
            {
                return 3;
            }
            //Distracted
            if ((!eyes_sleeping && pose_sleeping) || (eyes_sleeping && !pose_sleeping))
            {
                return 2;
            }
            //Using Phone
            if (phone_usage && !eyes_sleeping && !pose_sleeping)
            {
                return 1;
            }
            //Attentive
            return 0;
        }

        //Detect if the pose suggests sleeping
        public void pose_Sleeping(PXCMFaceData.PoseEulerAngles angles)
        {
            if (angles.pitch < -10)
            {
                phone_usage = true;
            }
            else
            {
                phone_usage = false;
            }

            if (angles.pitch > 35 || angles.pitch < -20 || angles.roll > 20 || angles.roll < -20 || angles.yaw > 25 || angles.yaw < -25)
            {
                pose_sleeping = true;
            }
            else
            {
                pose_sleeping = false;
            }
        }

        //Detect if the eyes suggest sleeping
        public void eyes_Sleeping(PXCMFaceData.ExpressionsData.FaceExpressionResult leftEye, PXCMFaceData.ExpressionsData.FaceExpressionResult rightEye)
        {
            bool eyesClosed = rightEye.intensity > 35 && leftEye.intensity > 35;
            if (frames.Count == 100)
            {
                frames.RemoveAt(0);
            }
            frames.Add(eyesClosed);

            double t = 0;
            for (int i = 0; i < frames.Count; i++)
            {
                if (frames[i])
                {
                    t++;
                }
            }
            if (t / frames.Count > threshold)
            {
                eyes_sleeping =  true;
            }
            else
            {
                eyes_sleeping  = false;
            }
        }
    }

    class Mqtt
    {
        string brokerAddress;
        string clientId;
        MqttClient client;
        DistractionDetection distractionDetection;
        public Mqtt(string address, DistractionDetection d)
        {
            brokerAddress = address;
            clientId = Guid.NewGuid().ToString();
            distractionDetection = d;
            init();
        }

        //Start and connect MqttClient
        public void init()
        {
            client = new MqttClient(brokerAddress);
            client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            client.Connect(clientId);
        }
        
        //On each message reception, send the level of distraction on topic /Distraction
        public void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string ReceivedMessage = Encoding.UTF8.GetString(e.Message);
            Console.WriteLine(ReceivedMessage);
            publish("/Distraction", (distractionDetection.isDistracted()).ToString());
        }

        public void subscribe(string topic)
        {
            client.Subscribe(new string[] { topic }, new byte[] { 2 });
        }

        public void publish(string topic, string msg)
        {
            byte[] message = Encoding.UTF8.GetBytes(msg);
            client.Publish(topic, message);
        }

    }
    static class Program
    {
        

        [STAThread]
        static void Main()
        {
            DistractionDetection distractionDetection = new DistractionDetection();

            //MQTT Setup
            Mqtt mqtt = new Mqtt("localhost", distractionDetection);
            string topic = "/STEP-DP";
            mqtt.subscribe(topic);
            mqtt.subscribe("/Distraction");

            //Create Session
            PXCMSession session = PXCMSession.CreateInstance();

            //If Session creation was successfull, create SenseManager, FaceModule and configuration
            if (session != null)
            {

                //Create SenseManager and initialize it
                PXCMSenseManager senseManager = PXCMSenseManager.CreateInstance();
                senseManager.EnableFace();
                senseManager.Init();

                //Create FaceModule
                PXCMFaceModule faceModule = senseManager.QueryFace();
                faceModule = senseManager.QueryFace();

                //Create Configuration
                PXCMFaceConfiguration cfg = faceModule.CreateActiveConfiguration();
                PXCMFaceConfiguration.ExpressionsConfiguration expc = cfg.QueryExpressions();
                expc.EnableAllExpressions();
                expc.Enable();
                cfg.ApplyChanges();

                //Get the number of faces each frame
                int xframe = 0;
                while (senseManager.AcquireFrame(true).IsSuccessful())
                {
                    xframe = xframe + 1;
                    //Get number of faces
                    PXCMFaceData faceData = faceModule.CreateOutput();
                    faceData.Update();
                    Int32 nfaces = faceData.QueryNumberOfDetectedFaces();

                    //For each face get the pose and expression data
                    for (int i = 0; i < nfaces; i++)
                    {
                        PXCMFaceData.Face face = faceData.QueryFaceByIndex(i);
                        //retrieve the pose information
                        PXCMFaceData.PoseData pdata = face.QueryPose();
                        PXCMFaceData.PoseEulerAngles angles;
                        pdata.QueryPoseAngles(out angles);
                        

                        //retrieve the expression information
                        PXCMFaceData.ExpressionsData edata = face.QueryExpressions();
                        if (edata != null)
                        {
                            PXCMFaceData.ExpressionsData.FaceExpressionResult leftEye;
                            edata.QueryExpression(PXCMFaceData.ExpressionsData.FaceExpression.EXPRESSION_EYES_CLOSED_LEFT, out leftEye);
                            PXCMFaceData.ExpressionsData.FaceExpressionResult rightEye;
                            edata.QueryExpression(PXCMFaceData.ExpressionsData.FaceExpression.EXPRESSION_EYES_CLOSED_RIGHT, out rightEye);

                            //Process Data
                            distractionDetection.processData(angles, leftEye, rightEye);
                            
                        }
                    }
                    if(xframe % 10 == 0)
                    {
                       mqtt.publish("/Distraction", (distractionDetection.isDistracted()).ToString());
                    }
                    senseManager.ReleaseFrame();
                }
                senseManager.Dispose();
                session.Dispose();
            }
        }
    }
}
