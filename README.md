# TOC-facetracking

The TOC-facetracking detects the attention of the driver based on the eyes state as well as the head pose data. 
It uses mqtt to publish the state of distraction. 
The Broker Address is "localhost", if it receives a message on the "/STEP-DP" topic, it sends out the distraction state on topic "/Distraction".

The response will either be 0,1,2 or 3. 
*  0 : Not distracted
*  1 : Might be looking on his phone 
*  2 : Slightly distracted (either eyes closed or face not facing the front)
*  3 : Highly distracted (probably sleeping)

## Installation

*  In order to get this c# project running, you need to have the RealSense 2016 R2 SDK installed on your system. 
You can download it from [here](https://software.intel.com/en-us/realsense-sdk-windows-eol) (at the bottom of the page).

*  Of course you also need to plug in the RealSense SR300 Camera. 

*  Just open the c# Solution in Visual Studio. In theory it should work right away. 

For a visualization of the live video augmented with the head pose data (yaw, pitch, roll) as well as indicators for the left and right eye state, visit [this repository](https://gitlab.com/s8sgenge/realsense-facetracking).

![](https://imgur.com/e43McmX.jpg)

